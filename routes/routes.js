"use strict";

const User = require('../models/User');

exports.login = function(req, res) {
  res.render('login');
}

exports.index = function(req, res) {
  let username = req.body.username;
  let password = req.body.password;
  User.findUser(username, password, function(err, data){
    if(err) {
      throw err;
    } else if(data[0]) {
      res.render('index', {loginuser: data[0].username});
    } else {
      res.redirect('/');
    }
  });
}

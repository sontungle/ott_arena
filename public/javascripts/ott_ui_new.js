/** View for OTT page */
/* @author: VuPhan */
function OttPage(){

    var me = this;

    //timeout variable
    me.challengeTimeout = null;
    me.battleTimeout = null;

    //semantic-ize the selectors
    var elements = {
        'sound'                          : '#sound',
        'userId'                         : '#username',
        'mainChat'                       : '.match-chat-main',
        'chatLog'                        : '.chat-content',
        'chatSubmit'                     : '#message-send',
        'rejectChallenge'                : '.js-button-reject',
        'acceptChallenge'                : '.js-button-accept',
        'chatVal'                        : '#message',
        'ranking'                        : '.ranking > .content',
        'ranking.nav'                    : '.ranking .nav-holder',
        'ranking.navContent'             : 'ranking-container', //special because the jPage lib requires it zz
        'personal.rank'                  : '.personal-stats  .current-rank',
        'personal.stats'                 : '.stats',
        'online'                         : '.online-list .content',
        'online.challenge'               : '.online-list .content .challenge',

        'template.chatLogItem'           : '#chat-log-item',
        'template.ranking'               : '#template-ranking',
        'template.stats'                 : '#template-stats',
        'template.online'                : '#template-online',
        'template.battle'                : '#template-battle',

        'matchOverlay'                   : '#match-overlay',
        'matchOverlay.content'           : '#match-overlay .modal-content',
        'matchOverlay.playerInput'       : '#match-overlay .player-input',
        'matchOverlay.playerHandInput'   : '#match-overlay .battle-hand-input',
        'matchOverlay.playerHandBack'    : '#match-overlay .battle-hand-back',
        'matchOverlay.countdownContainer': '#match-overlay .countdown',
        'matchOverlay.countdown'         : '#match-overlay .countdown-val',
        'matchOverlay.resultContainer'   : '#match-overlay .results',
        'matchOverlay.resultImg'         : '#match-overlay .score-img',
        'matchOverlay.resultText'        : '#match-overlay .score-text',
        'matchOverlay.resultItem'        : '#match-overlay .battle-hand-item',
        'matchOverlay.finalResultText'   : '#match-overlay .results-text',
        'matchOverlay.matchChat'         : '#match-overlay .match-chat',
        'matchOverlay.matchChatVal'      : '#match-overlay .match-chat input',
        'battleInput'                    : '#match-overlay .battle-input',
        'battleSubmit'                   : '#match-overlay .battle-submit'
    };

    //storage of handlers
    var handlers = {
        onIdentityRequest: function(result){
            me.chatApp.sendIdentity(me.username);
        },
        onReceiveName: function(result){
            if ( result.success ){
                message = 'Welcome to hell, ' + result.name + '!';
                appendMessage('system', {
                    message: message
                });
            }
        },
        onUpdateRanking: function(data){

            //cache data to use again
            me.rankingData = {};

            //prepare metadataaaaa
            for ( var i = 0; i < data.length; i++ ){
                data[i].rank = i + 1;
                data[i].title = getTitleFromRanking(i+1);
                data[i].top = ( i <= 2 ) ? true: false;
                data[i].isSelf = (data[i].username == me.username)? true: false;

                if ( typeof(data[i].win) != 'undefined' && typeof(data[i].lose) != 'undefined' && ( parseInt(data[i].win, 10) > 0 || parseInt(data[i].lose, 10) > 0 ) ){
                    ratio = parseInt(data[i].win, 10) / ( parseInt(data[i].win, 10) + parseInt(data[i].lose, 10) );
                    data[i].roundedRatio = ( Math.round(ratio*10000) / 100 ) + '%';
                    }else{
                    data[i].roundedRatio = 'N/A';
                }

                me.rankingData[data[i].username] = data[i];
            }
            var context = {
                data: data
            };
            var htmlContent = me.templates.ranking(context);
            elm('ranking').html(htmlContent);

            //setup paginators
            elm('ranking.nav').jPages({
                containerID: elements['ranking.navContent'],
                perPage: 8
            });

            //get the ranking for current user, to show in personal section
            //can make it very easy by copy the row on ranking section
            elm('personal.rank').addClass('hidden');

            elm('personal.rank').html( elm('ranking').find('table').clone() );

            //remove all tds, then add back the row of current user
            elm('personal.rank').find('tbody > tr').remove();

            elm('personal.rank').find('tbody').html( elm('ranking').find('tr[js-name=' + me.username + ']').clone() );

            elm('personal.rank').removeClass('hidden');

            //fix the opacity from the library of paginator
            elm('personal.rank').find('tr').css({
                opacity: 1,
                display: 'table-row'
            });
        },
        onNewMessage: function(message){
            if ( typeof(message) == 'string' ){
                appendMessage('system', {
                    message: message
                });
            }else{
                //player specific
                appendMessage('player', {
                    message: message.message,
                    player: message.user
                });
            }
        },
        //submit chat
        onSubmitChatViaButton: function(e){
            //get correct chatVal element
            var chatElm = $(e.currentTarget);
            handlers._onSubmitChat(chatElm);
        },
        onSubmitChatViaEnter: function(e){
            if ( e.keyCode === 13 ){ //enter key code
                var chatElm = $(e.currentTarget).closest('.chat-input').find('input');
                handlers._onSubmitChat(chatElm);
            }
        },
        _onSubmitChat: function(chatElm){
            var message = jQuery.trim( chatElm.val() ) ; //no more mind game lol
            if ( message !== '' ){
                //if it is a command
                //do it later lol
                if ( message.charAt(0) === '/' ){
                    var returnMsg = me.chatApp.processCommand(message);
                    if ( !!returnMsg ){
                        appendMessage('system', {
                            message: returnMsg
                        });
                    }
                }else{
                    me.chatApp.sendMessage(message);
                }
            }
            //clear input
            chatElm.val('');
        },

        //battle submit

        //submit chat
        onSubmitBattleViaButton: function(e){
            handlers._onSubmitBattle();
        },
        onSubmitBattleViaEnter: function(e){
            if ( e.keyCode === 13 ){ //enter key code
                handlers._onSubmitBattle();
            }
        },
        _onSubmitBattle: function(){
            var message = jQuery.trim( elm('battleInput').val() ) ; //no more mind game lol
            if ( message !== '' ){
                if ( isValidHand(message) ){
                    me.chatApp.sendHand(message);
                    elm('matchOverlay.playerInput').fadeOut(200, function(){
                        elm('matchOverlay.matchChatVal').focus();
                    });
                }
            }
            //clear input
            elm('battleInput').val('');
        },
        onUpdatePersonal: function(data){
            console.log(data);
            //get own row of users
            var ratio;
            var roundedRatio;

            if ( typeof(data.win) != 'undefined' && typeof(data.lose) != 'undefined' && ( parseInt(data.win, 10) > 0 || parseInt(data.lose, 10) > 0 ) ){
                ratio = parseInt(data.win, 10) / ( parseInt(data.win, 10) + parseInt(data.lose, 10) );
                roundedRatio = ( Math.round(ratio*10000) / 100 ) + '%';
            }else{
                roundedRatio = 'N/A';
            }
            var context = {
                ratio: roundedRatio,
                ccn: data.ccn? data.ccn: 0
            };
            var htmlContent = me.templates.stats(context);
            elm('personal.stats').html(htmlContent);
        },
        onUpdateOnlineUsers: function(currentUsers){
            //create a nicer format for data
            var users = [];
            for ( var i in currentUsers){
                users.push({
                    'user': currentUsers[i],
                    'self': currentUsers[i] == me.username? true: false
                });
            }
            var context = {
                users: users
            };
            var htmlContent = me.templates.online(context);
            elm('online').html(htmlContent);
        },
        onChallenge: function(message){
            appendMessage('system', {
                message: message
            });
            //get two nice buttons to do some shit about accepting or rejecting
            appendMessage('system', {
                message:
                '<div class="challenge-receive-button"><button class="btn btn-default btn-sm js-button-accept">Kill him!</button>&nbsp;&nbsp;\
                <button class="btn btn-default btn-sm js-button-reject">No I\'m scared :(</button></div>'
            });
            clearTimeout(me.challengeTimeout);
            me.challengeTimeout = setTimeout(function(){
                me.chatApp.sendTimeout();
                //appendMessage('system', {
                //    message: 'Response timeout'
                //});
            }, 15000); //15 sec
            //play sound, yay
            playSound('notify');
        },
        onMatchDone: function(){
            clearTimeout(me.challengeTimeout);
            clearTimeout(me.battleTimeout);
            document.title = me.pageTitle;
        },
        onMatchStart: function(data){
            console.log(me.rankingData);
            console.log(data);

            var battleTime = 30000;

            //build up everything that the overlay needed
            var p1Data = me.rankingData[data.player1];
            var p2Data = me.rankingData[data.player2];

            var context = {
                p1: p1Data,
                p2: p2Data,
                isPlaying: (p1Data.username == me.username || p2Data.username == me.username)
            };

            var htmlContent = me.templates.battle(context);
            elm('matchOverlay.content').html(htmlContent);

            //clone the chatbox!!!!!
            elm('mainChat').clone().removeClass().appendTo( elm('matchOverlay.matchChat') );

            //show the match overlay
            elm('matchOverlay').one('shown.bs.modal', function(){
                //a little cheator, calling a blank message to have the chatbox scrolled
                appendMessage('system', '');
                //a player will have his battle chatbox focused
                if ( context.isPlaying === true ){
                    elm('battleInput').focus();
                }else{
                    //an observer will have his chatbox focused instead
                    elm('.matchOverlay.matchChatVal').focus();
                }
            });
            elm('matchOverlay').modal('show');

            //countdown
            //and also set a timeout for challenge
            if ( context.isPlaying === true ){
                clearTimeout(me.battleTimeout);
                me.battleTimeout = setTimeout(function(){
                    me.chatApp.sendTimeout();
                }, battleTime); //60 sec
            }

            var battleTimeText = battleTime / 1000;
            //first run
            elm('matchOverlay.countdown').html(battleTimeText);
            battleTimeText--;
            clearInterval(me.countdownInterval);
            me.countdownInterval = setInterval(function(){
                elm('matchOverlay.countdown').html(battleTimeText);
                battleTimeText--;
                if ( battleTimeText < 0) {
                    clearInterval(me.countdownInterval);
                }
            }, 1000); //once per sec

            //update title to LIVE!!!
            document.title = p1Data.username + ' vs ' + p2Data.username + ' LIVE !!! ' + me.pageTitle;

        },
        onHandSubmitted: function(){
            clearTimeout(me.challengeTimeout);
            clearTimeout(me.battleTimeout);
        },
        onMatchCancel: function(){
            elm('matchOverlay').modal('hide');
            elm('matchOverlay.content').html('');
            elm('acceptChallenge').parent().remove();
            clearTimeout(me.challengeTimeout);
            clearTimeout(me.battleTimeout);
            setTimeout( function(){
                elm('chatVal').focus();
            }, 200);
            //reset title
            document.title = me.pageTitle;
        },
        onChallengeClick: function(e){
            var currentElm = $(e.currentTarget);
            var name = currentElm.attr('js-name');
            me.chatApp.sendChallenge(name);
        },
        onMatchFinish: function(data){
            //console.log(data);
            //clear countdown interval
            clearInterval(me.countdownInterval);
            //eventually hide it
            elm('matchOverlay.countdownContainer').fadeOut(300, function(){
                handlers._onShowHandStart(data);
            });
            clearTimeout(me.challengeTimeout);
            clearTimeout(me.battleTimeout);
        },
        _onShowHandStart: function(data){
            //first, generate the hand and put the content inside html
            //find out who's the winner and who lose
            var winner, loser, winner_hand, loser_hand;
            if ( data.type == 'Not Draw'){
                winner = data.winner;
                loser = data.loser;
                winner_hand = data.winner_hand;
                loser_hand = data.loser_hand;
            }else{
                winner = data.player1;
                loser = data.player2; //it doesnt matter who wins or lose
                winner_hand = data.hands;
                loser_hand = data.hands;
            }
            var winnerText = $( elements['matchOverlay.resultText'] + '[js-name=' + winner + ']' );
            var winnerImg = $( elements['matchOverlay.resultImg'] + '[js-name=' + winner + ']' );
            var loserText = $( elements['matchOverlay.resultText'] + '[js-name=' + loser + ']' );
            var loserImg = $( elements['matchOverlay.resultImg'] + '[js-name=' + loser + ']' );

            generateHand(winnerImg, winner_hand);
            generateHand(loserImg, loser_hand);

            var scoreProg = generateScoreProgress(winner_hand, loser_hand);

            //when everything is set
            elm('matchOverlay.resultContainer').removeClass('hidden');

            revealHand(1, 1200,   scoreProg, winnerText, loserText);
            revealHand(2, 2700,  scoreProg, winnerText, loserText);
            revealHand(3, 4200,  scoreProg, winnerText, loserText);
            revealHand(4, 5700,  scoreProg, winnerText, loserText);
            revealHand(5, 7200,  scoreProg, winnerText, loserText);

            //Text to show
            var finalText = '';
            if ( data.type == 'Not Draw'){
                if ( !!data.need_last_hit ){
                    finalText = data.winner + ' last hit!!!';
                }else{
                    finalText = data.winner + ' won!!!';
                }
            }else{
                finalText = 'Draw!!!';
            }
            //this needs to be after last hand reveal
            setTimeout( function(){
                elm('matchOverlay.finalResultText').html(finalText).fadeIn(500);
            }, 7300);

            //after that about 3 seconds, we close the modal
            setTimeout( function(){
                elm('matchOverlay').modal('hide');
                //clear contents by the way
                elm('matchOverlay.content').html('');
                //push the message to the chat too
                appendMessage('system', {
                    message: finalText
                });
                me.chatApp.requestUpdate();
                //focus back on the text box
                setTimeout( function(){
                    elm('chatVal').focus();
                }, 200);
                //reset title
                document.title = me.pageTitle;
            }, 10300);
        },
        onChallengeRes: function(result){
            appendMessage('system', {
                message: result.message
            });
        },
        onAcceptButtonClick: function(){
            me.chatApp.sendResponse('accept');
            //remove buttons since they do nothing good now lol
            elm('acceptChallenge').parent().remove();
        },
        onRejectButtonClick: function(){
            me.chatApp.sendResponse('reject');
            elm('acceptChallenge').parent().remove();
        },
        notifyChallengeStart: function(data){
            //update the title
            document.title = data.player1 + ' vs ' + data.player2 + ' coming !!! ' + me.pageTitle;
        },
        onPlayerHandInput: function(e){
            //detect what hand to be inserted
            var currentElm = $(e.currentTarget);
            var hand = currentElm.attr('js-hand');

            //get the current input's content, append the hand into it
            var currentHand = elm('battleInput').val();
            //if current hand seems to be full, stop inserting
            currentHand = jQuery.trim(currentHand);
            if ( currentHand.length >= 9 ){
                elm('battleInput').focus();
                return;
            }
            currentHand = currentHand + ' ' + hand;
            elm('battleInput').val(currentHand).focus();
        },
        onPlayerHandBack: function(){
            var currentHand = elm('battleInput').val();
            //if current hand seems to be full, stop inserting
            currentHand = jQuery.trim(currentHand);
            //if nothing to be found, who cares
            if ( currentHand.length === 0 ){
                elm('battleInput').focus();
                return;
            }
            var handArr = currentHand.split(' ');
            if ( handArr.length > 0 ){
                handArr.splice(handArr.length - 1, 1); //remove the last element
            }
            //join back the chopped hands lol
            currentHand = handArr.join(' ');
            elm('battleInput').val(currentHand).focus();
        }
    };

    function elm(selector){
        return $(elements[selector]);
    }

    function bindEvents(){
        //when requested a name
        me.socket.on('identity request', handlers.onIdentityRequest);
        //when the name is sent back from server
        me.socket.on('nameResult', handlers.onReceiveName);
        //update ranked
        me.socket.on('updateranking', handlers.onUpdateRanking);
        //chat chit
        me.socket.on('message', handlers.onNewMessage);
        //challenge result
        me.socket.on('challengeResult', handlers.onChallengeRes);
        //send own's chat
        $(document).on('click', elements['chatSubmit'], handlers.onSubmitChatViaButton);
        //same thing, but by pressing enter on the form
        $(document).on('keyup', elements['chatVal'], handlers.onSubmitChatViaEnter);
        //battle input
        $(document).on('click', elements['battleSubmit'], handlers.onSubmitBattleViaButton);
        //same thing, but by pressing enter on the form
        $(document).on('keyup', elements['battleInput'], handlers.onSubmitBattleViaEnter);
        //update personal stats
        me.socket.on('updatepersonal', handlers.onUpdatePersonal);
        //update user online
        me.socket.on('onlineUsers', handlers.onUpdateOnlineUsers);
        //issue a challenge
        me.socket.on('challenge', handlers.onChallenge);
        //clear timeout when a match is finished
        me.socket.on('recieve result', handlers.onMatchDone);
        //when a challenge is accepted
        me.socket.on('matchStart', handlers.onMatchStart);
        //cancel a challenge due to disconnection
        me.socket.on('matchCancelled', handlers.onMatchCancel);
        //click on a name's sword to challenge
        $(document).on('click', elements['online.challenge'], handlers.onChallengeClick);
        //result of the match
        me.socket.on('match result', handlers.onMatchFinish);
        //if hand submitted then clear timeout
        me.socket.on('hand receive', handlers.onHandSubmitted);
        //buttons to accept / reject challenge
        $(document).on('click', elements['acceptChallenge'], handlers.onAcceptButtonClick);
        $(document).on('click', elements['rejectChallenge'], handlers.onRejectButtonClick);
        //when the challenge is inited, update the title!!!
        me.socket.on('challenge.init', handlers.notifyChallengeStart);
        //click on any input hand => update player input for battle
        $(document).on('click', elements['matchOverlay.playerHandInput'], handlers.onPlayerHandInput);
        //click on remove one hand
        $(document).on('click', elements['matchOverlay.playerHandBack'], handlers.onPlayerHandBack);
    }


    /************** Data support function *************

    /*get the proper ranking */
    function getTitleFromRanking(rank){
        switch ( rank ){
            case 1:
                return 'dragon';
            case 2:
                return 'cyclops';
            case 3:
                return 'knight';
            case 4:
                return 'minotaur';
            case 5:
                return 'mage';
            case 6:
                return 'cerberus';
            case 7:
                return 'clown';
            case 8:
                return 'imp';
            case 9:
                return 'chicken';
            default:
                return 'shit';
        }
    }

    /* determine if an input string is a valid hand */
    function isValidHand(message) {
        var acceptHands = {'p': true, 'r': true, 's': true};
        var parts = message.split(' ');
        if(parts.length != 5) {
            return false;
        }
        for(var i = 0; i < parts.length; i++) {
            if(!(parts[i] in acceptHands)) {
                return false;
            }
        }
        return true;
    }

    function compareHand(hand1, hand2) {
        if(hand1 == hand2) {
            return 0;
        } else if (hand1 == 'r') {
            if(hand2 == 'p') {
                return -1;
            } else {
                return 1;
            }
        } else if(hand1 == 'p') {
            if(hand2 == 'r') {
                return 1;
            } else {
                return -1;
            }
        } else {
            if(hand2 == 'p') {
                return 1;
            } else {
                return -1;
            }
        }
    }

    function generateScoreProgress(winner, loser){
        var winnerArr = winner.split(' ');
        var loserArr = loser.split(' ');

        var result = [];

        for ( var i = 0; i < 5; i++ ){
            result.push( compareHand(winnerArr[i], loserArr[i]) );
        }
        return result;
    }

    /* get current score */
    function getCurrentScore(order, progress){
        var result = {
            win: 0,
            lose: 0
        };
        for ( var i = 0; i <= order; i++ ){
            if ( progress[i] == 1 ){
                result.win = result.win + 1;
            }else if ( progress[i] == -1 ){
                result.lose = result.lose + 1;
            }
        }
        return result;
    }

    /************** UX function ************/

    /* print a message to the chat log, depends on type */
    function appendMessage(type, data){

        if ( data !== '' ){
            //building up meta data for handlebar
            isSystem = type == 'system'? true: false;
            isUser = type == 'player'? true: false;
            var context = {
                isSystem: isSystem,
                isUser: isUser,
                data: data
            };

            var htmlContent = me.templates.chatLogItem(context);
            elm('chatLog').append(htmlContent);
        }
        //scroll the div to its very end
        jQuery.each( elm('chatLog'), function(idx, elm){
            $(elm).animate({
                scrollTop: elm.scrollHeight
            }, 100);
        });
    }

    /* get a hand to its corresponding image */
    function translateHandToImg(handItem){
        switch(handItem){
            case 'r':
                return 'images/punch.png';
            case 'p':
                return 'images/hi.png';
            case 's':
                return 'images/fingerscrossed.png';
        }
    }

    /* generate all hands result, but hide all of them, to reveal one by one */
    function generateHand(selector, hand){
        var handArr = hand.split(' ');
        console.log(handArr);
        for ( var i = 0; i < handArr.length; i++ ){
            selector.append(
                '<img src="' + translateHandToImg( handArr[i] ) + '" class="battle-hand-item" js-order="'+  (i+1) + '"/>' + ( i === (handArr.length - 1)? '' : '&nbsp;' )
            );
        }
    }

    /* reveal */
    function revealHand(order, time, progress, winSelector, loseSelector){
        setTimeout(function(){
            //show hand with order
            $( elements['matchOverlay.resultItem'] + '[js-order=' + order + ']' ).fadeIn(400);
            //get current result
            var curScore = getCurrentScore(order-1, progress);
            winSelector.html(curScore.win);
            loseSelector.html(curScore.lose);
        }, time);
    }

    //yessssssssssssssss
    //this variable is for storing any sounds to be used
    var sounds = {
        'notify': '/sounds/coin.mp3'
    };

    //play a sound by the name defined
    function playSound(sound){
        var filename = sounds[sound];
        //elm('sound')[0].innerHTML = '<embed src="' + filename + '" hidden="true" autostart="true" loop="false" enablejavascript="true"/>';
        //here's the new way of doing things
        if ( Audio ){
            var soundItem = new Audio(filename);
            soundItem.play();
        }
    }


    /***************** initialize ****************/
    function init(){

        me.username = elm('userId').val();

        me.socket = io.connect();
        me.chatApp = new Chat(me.socket);

        //setup handlebar templates
        me.templates = {};

        //chat log item
        var source = elm('template.chatLogItem').html();
        me.templates.chatLogItem = Handlebars.compile(source);

        //ranking board
        source = elm("template.ranking").html();
        me.templates.ranking = Handlebars.compile(source);

        //personal stats
        source = elm("template.stats").html();
        me.templates.stats = Handlebars.compile(source);

        //online users
        source = elm('template.online').html();
        me.templates.online = Handlebars.compile(source);

        //match overlay content
        source = elm('template.battle').html();
        me.templates.battle = Handlebars.compile(source);

        bindEvents();

        //get default title
        me.pageTitle = document.title;

    }

    init();

    return {

    };
}


/* onload */
$(document).ready(function(){
    window.ottPage = new OttPage();
});

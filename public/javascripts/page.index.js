$(document).ready(function(){

    var introElm = $('.intro');
    var loginElm = $('.login');
    var footerElm = $('.footer');

    //fade the title in
    introElm.find('.title').removeClass('invi');
    setTimeout(function(){
        introElm.find('.subtitle').removeClass('invi');
    }, 1000);

    //2s timeout => show the login

    function showLogin(){
        introElm.css({
            'left' : 0
        }).removeClass('col-md-push-3'); //stupid jquery (rofl)

        setTimeout(function(){
            loginElm.fadeIn(1000);
        }, 500);

        setTimeout(function(){
            footerElm.fadeIn(1000);
        }, 1500);
    }

    setTimeout(function(){
        showLogin();
    }, 2000);

    //trigger form when link is clicked
    $('a.submit').on('click', function(){
        $('.login form').submit();
    });

    //entering on password field as well
    $('#password').on('keyup', function(event){
        if ( event.keyCode === 13 ){
            //enter key
            $('.login form').submit();
        }
    });

});
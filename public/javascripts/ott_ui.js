function isValidHand(message) {
  var acceptHands = {'p': true, 'r': true, 's': true};
  var parts = message.split(' ');
  if(parts.length != 5) {
    return false;
  }
  for(var i = 0; i < parts.length; i++) {
    if(!(parts[i] in acceptHands)) {
      return false;
    }
  }
  return true;
}
function divEscapedContentElement(message) {
    return $('<div></div>').text(message);
}
function divSystemContentElement(message) {
    return $('<div></div>').html('<i>' + message + '</i>');
}
function processUserInput(chatApp, defaulttext) {
  var message = defaulttext || $('#send-message').val();
  if(message) {
    if(message.charAt(0) === '/') {
      var returnMessage = chatApp.processCommand(message);
      if(returnMessage) {
        $('#message').append(divSystemContentElement(returnMessage));
      }
    } else if(isValidHand(message)) {
      chatApp.sendHand(message);
    } else {
      chatApp.sendMessage(message);
      $('#message').scrollTop($('#message').prop('scrollHeight'));
    }
    $('#send-message').val('');
  }
}

var dragon = '<span><img src="http://elasticbeanstalk-us-west-2-254492908186.s3-website-us-west-2.amazonaws.com/home/ott_platform_static_files/images/dragon.png" height="32" width="32" /></span>';
      var knight = '<span><img src="http://elasticbeanstalk-us-west-2-254492908186.s3-website-us-west-2.amazonaws.com/home/ott_platform_static_files/images/knight.png" height="32" width="32" /></span>';
      var mage = '<span><img src="http://elasticbeanstalk-us-west-2-254492908186.s3-website-us-west-2.amazonaws.com/home/ott_platform_static_files/images/mage.png" height="32" width="32" /></span>';
      var imp = '<span><img src="http://elasticbeanstalk-us-west-2-254492908186.s3-website-us-west-2.amazonaws.com/home/ott_platform_static_files/images/imp.png" height="32" width="32" /></span>';
      var chicken = '<span><img src="http://elasticbeanstalk-us-west-2-254492908186.s3-website-us-west-2.amazonaws.com/home/ott_platform_static_files/images/chicken.png" height="32" width="32" /></span>';
      var clown = '<span><img src="http://elasticbeanstalk-us-west-2-254492908186.s3-website-us-west-2.amazonaws.com/home/ott_platform_static_files/images/clown.png" height="32" width="32" /></span>';
      var shit = '<span><img src="http://elasticbeanstalk-us-west-2-254492908186.s3-website-us-west-2.amazonaws.com/home/ott_platform_static_files/images/shit.png" height="32" width="32" /></span>';
      var cyclops = '<span><img src="http://elasticbeanstalk-us-west-2-254492908186.s3-website-us-west-2.amazonaws.com/home/ott_platform_static_files/images/Cyclops.png" height="32" width="32" /></span>';
      var cerberus = '<span><img src="http://elasticbeanstalk-us-west-2-254492908186.s3-website-us-west-2.amazonaws.com/home/ott_platform_static_files/images/cerberus.png" height="32" width="32" /></span>';
      var minotaur = '<span><img src="http://elasticbeanstalk-us-west-2-254492908186.s3-website-us-west-2.amazonaws.com/home/ott_platform_static_files/images/Minotaur.png" height="32" width="32" /></span>';

var socket = io.connect();
$(document).ready(function() {
  var timeOut;
  var chatApp = new Chat(socket);
  socket.on('identity request', function() {
    chatApp.sendIdentity($('#username').val());
  });
  socket.on('nameResult', function(result) {
    if(result.success) {
      message = 'Welcome  ' + result.name;
    }
    $('#message').append(divSystemContentElement(message));
  });
  socket.on('message', function(message){
    var newElement = $('<div></div>').text(message);
    $('#message').append(newElement);
    $('#message').scrollTop($('#message').prop('scrollHeight'));
  });
  socket.on('onlineUsers', function(currentUsers) {
    $('#users-list').empty();
    $('#users-list').append(divSystemContentElement('Online User'));
    for(var user_id in currentUsers ) {
      $('#users-list').append(divEscapedContentElement(currentUsers[user_id]));
    }
  });
  socket.on('challengeResult', function(result) {
    $('#message').append(divSystemContentElement(result.message));
    $('#message').scrollTop($('#message').prop('scrollHeight'));
  });
  socket.on('challenge', function(message) {
    $('#message').append(divSystemContentElement(message));
    timeOut = setTimeout(function() {
      $('#message').append(divSystemContentElement('Response timeout'));
      chatApp.sendTimeout();
    }, 30000);
    $('#message').scrollTop($('#message').prop('scrollHeight'));
  });
  socket.on('recieve result', function() {
    clearTimeout(timeOut);
  });
  socket.on('hand request', function(message) {
    $('#message').append(divSystemContentElement(message));
    timeOut = setTimeout(function() {
      chatApp.sendTimeout();
    }, 60000);
    $('#message').scrollTop($('#message').prop('scrollHeight'));
  });
  socket.on('hand receive', function() {
    clearTimeout(timeOut);
  });
  socket.on('not in match hand', function() {
    $('#message').append(divSystemContentElement('You are not in match to play hands'));
    $('#message').scrollTop($('#message').prop('scrollHeight'));
  });
  socket.on('match result', function(data) {
    for(var attribute in data) {
        alert(attribute + ':' + data[attribute]);
    }
  });
  socket.on('updateranking', function(data) {
      $('#ranking').empty();
      $('#ranking').empty();
      $('#ranking').append(divEscapedContentElement('Ranking'));
      for(var i = 0; i < data.length; i++) {
          if(i == 10) break;
          switch(i) {
              case 0:
                $('#ranking').append((i + 1) + '. ' + data[i].username + ' ' + data[i].win + ' ' + data[i].lose + dragon + '<br />');
                break;
              case 1:
                $('#ranking').append((i + 1) + '. ' + data[i].username + ' ' + data[i].win + ' ' + data[i].lose + cyclops + '<br />');
                break;
              case 2:
                $('#ranking').append((i + 1) + '. ' + data[i].username + ' ' + data[i].win + ' ' + data[i].lose + knight + '<br />');
                break;
              case 3:
                $('#ranking').append((i + 1) + '. ' + data[i].username + ' ' + data[i].win + ' ' + data[i].lose + minotaur + '<br />');
                break;
              case 4:
                $('#ranking').append((i + 1) + '. ' + data[i].username + ' ' + data[i].win + ' ' + data[i].lose + mage + '<br />');
                break;
              case 5:
                $('#ranking').append((i + 1) + '. ' + data[i].username + ' ' + data[i].win + ' ' + data[i].lose + cerberus + '<br />');
                break;
              case 6:
                $('#ranking').append((i + 1) + '. ' + data[i].username + ' ' + data[i].win + ' ' + data[i].lose + clown + '<br />');
                break;
              case 7:
                $('#ranking').append((i + 1) + '. ' + data[i].username + ' ' + data[i].win + ' ' + data[i].lose + imp + '<br />');
                break;
              case 8:
                $('#ranking').append((i + 1) + '. ' + data[i].username + ' ' + data[i].win + ' ' + data[i].lose + chicken + '<br />');
                break;
              default:
                $('#ranking').append((i + 1) + '. ' + data[i].username + ' ' + data[i].win + ' ' + data[i].lose + shit + '<br />');
                break;
          }
      }
  });
  socket.on('updatepersonal', function(data) {
    $('#personal').empty();
    $('#personal').append(divEscapedContentElement('Your Record'));
    $('#personal').append(divEscapedContentElement('Name: ' + data.username));
    $('#personal').append(divEscapedContentElement('Win: ' + data.win));
    $('#personal').append(divEscapedContentElement('Lose: ' + data.lose));
    $('#personal').append(divEscapedContentElement('Streak: ' + data.streak));
  });
  $('#send-message').focus();
  $('#send-form').submit(function() {
    processUserInput(chatApp);
    $('#send-message').focus();
    return false;
  });
});

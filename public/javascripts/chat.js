var Chat = function(socket) {
  this.socket = socket;
}

Chat.prototype.processCommand = function(message) {
  var returnMessage = false;
  var command_tokens = message.split(' ');
  var first_tokens = command_tokens[0].substring(1, command_tokens[0].length);
  switch(first_tokens) {
    case 'challenge':
    case 'c':
      command_tokens.shift();
      var name = command_tokens.join(' ');
      this.sendChallenge(name);
      break;
    case 'reject':
    case 'r':
      this.sendResponse('reject');
      break;
    case 'accept':
    case 'a':
      this.sendResponse('accept');
      break;
    default:
      returnMessage = 'Command not found!';
      break;
  }

  return returnMessage;
}

Chat.prototype.sendMessage = function(message) {
  this.socket.emit('message', message);
}

Chat.prototype.sendChallenge = function(name) {
  this.socket.emit('challenge', {name: name});
}

Chat.prototype.sendResponse = function(message) {
  this.socket.emit('challengeRespone', {result: message});
}

Chat.prototype.sendHand = function(hand) {
  this.socket.emit('hand response', hand);
}

Chat.prototype.sendIdentity = function(identity) {
  this.socket.emit('identity response', identity);
}

Chat.prototype.sendTimeout = function() {
  this.socket.emit('timeout');
}

Chat.prototype.requestUpdate = function(){
  this.socket.emit('requestUpdate');
}

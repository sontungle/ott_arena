/* User model for OTT
 * @author Lic_uno
 */
"use strict";

const db = require('../lib/db');

let UserSchema = new db.Schema({
  username: {type: String, unique: true},
  password: String,
  win: Number,
  lose: Number,
  streak: Number
});

let OttUser = db.mongoose.model('User', UserSchema);

function findUser(username, password, callback) {
  OttUser.find({"username": username, "password": password}, function(err, data) {
    if(err) {
      callback(err);
    } else {
      callback(null, data);
    }
  });
}

function findMostWin(callback) {
  OttUser.find({"win": {$gte: 1}}).select('username win lose').sort({"win": -1, "lose": 1}).limit(1).exec(function(err, data) {
    callback(err, data);
  });
}

function findMostLose(callback) {
  OttUser.find({"lose": {$gte: 1}}).select('username win lose').sort({"lose": -1, "win": 1}).limit(1).exec(function(err, data) {
    callback(err, data);
  });
}

function findMostCCN(callback) {
  OttUser.find({"ccn": {$gte: 1}}).select('username ccn win lose').sort({"ccn": -1, "lose": -1, "win": 1}).limit(1).exec(function(err, data) {
    callback(err, data);
  });
}

function findYourRecord(username, callback) {
  OttUser.find({"username": username}).select('username win lose streak').limit(1).exec(function(err, data) {
    callback(err, data);
  });
}

function updateWin(username, callback) {
  OttUser.update({"username": username}, {$inc: {"win": 1}}, function(err, data) {
    callback(err, data);
  });
}

function updateLose(username, callback) {
  OttUser.update({"username": username}, {$inc: {"lose": 1}}, function(err, data) {
    callback(err, data);
  });
}

function updateStreakWin(username, callback) {
  OttUser.findOne({"username": username}, function(err, data) {
    if(err) throw err;
    data.streak += 1;
    data.save(function(err, data) {
      callback(err, data);
    });
  });
}

function updateStreakLose(username, callback) {
  OttUser.findOne({"username": username}, function(err, data) {
    if(err) throw err;
    let oldStreak = data.streak;
    data.streak = 0;
    data.save(function(err, data) {
      callback(err, data, oldStreak);
    });
  });
}

function findAll(callback) {
    OttUser.find({}).select('username win lose streak').exec(function(err, data) {
        callback(err, data);
    });
}

module.exports.findYourRecord = findYourRecord;
module.exports.updateLose = updateLose;
module.exports.updateWin = updateWin;
module.exports.findUser = findUser;
module.exports.updateStreakWin = updateStreakWin;
module.exports.updateStreakLose = updateStreakLose;
module.exports.findAll = findAll;

/* OTT Server
 * @author: Lic_uno
 */
"use strict";
const socketio = require('socket.io'),
      User = require('../models/User');
let io,
    currentUsers = {},
    namesUsed = [],
    currentUsersID = {},
    challenger = [],
    challengee = [],
    gladiators = [],
    hands = [];

exports.listen = function(server) {
  io = socketio.listen(server);
  io.set('log level', 2);
  io.sockets.on('connection', function(socket) {
    socket.emit('identity request');
    assignName(socket);
    handleOnlineUsers();
    handleMessage(socket);
    handleChallenges(socket);
    handleChallengesRespones(socket);
    handleMatch(socket);
    handleDisconnect(socket);
    handleTimeOut(socket);
    handleUpdateRequest(socket);
  });
};

function assignName(socket) {
  socket.on('identity response', function(identity) {
    let name = identity;
    if(name in currentUsersID) {
      socket.emit('message', 'This account has been logged in somewhere else. You now disconnect');
      socket.disconnect();
    } else {
      currentUsers[socket.id] = name;
      currentUsersID[name] = socket.id;
      socket.emit('nameResult', {
        success: true,
        name: name
      });
      namesUsed.push(name);
      socket.broadcast.emit('message', currentUsers[socket.id] + ' has joined');
      handleOnlineUsers();
      handleRecords(socket);
      handlePersonalRecord(socket);
    }
  });
}

function handleMessage(socket) {
  socket.on('message', function(message) {
    //io.sockets.emit('message', currentUsers[socket.id] + ': ' + message);
    io.sockets.emit('message', {
      user: currentUsers[socket.id],
      message: message
    });
  });
}

function handleDisconnect(socket) {
  socket.on('disconnect', function() {
    if(currentUsers[socket.id]) {
    io.sockets.emit('message', currentUsers[socket.id] + ' has left');
    let index = namesUsed.indexOf(currentUsers[socket.id]);
    if(challengee.length > 0 && (namesUsed[index] == challengee[0] || namesUsed[index] == challenger[0])) {
      challengee.length = 0;
      challenger.length = 0;
      io.sockets.emit('message', 'Challenge cancel due to player left');
      io.sockets.emit('matchCancelled');
    } else if(gladiators.length > 0 && (namesUsed[index] == gladiators[0] || namesUsed[index] == gladiators[1])) {
      gladiators.length = 0;
      hands.length = 0;
      io.sockets.emit('message', 'Match cancel due to player left');
      io.sockets.emit('matchCancelled');
    }

    delete namesUsed[index];
    delete currentUsersID[currentUsers[socket.id]];
    delete currentUsers[socket.id];
    handleOnlineUsers();
    }
  });
}

function handleChallenges(socket) {
  socket.on('challenge', function(user) {
    if(challenger.length > 0 || gladiators.length > 0) {
      socket.emit('challengeResult', {
        result: 'fail',
        message: 'There is another challenge, please wait for it to finish'
      });
    } else {
      let name = user.name;
      let index = namesUsed.indexOf(name);
      if(index == -1) {
        socket.emit('challengeResult', {
          result: 'fail',
          message: 'User ' + name + ' is not online at the moment'
        });
      } else if (name == currentUsers[socket.id]) {
        socket.emit('challengeResult', {
          result: 'fail',
          message: 'You cannot challenge yourself mister'
        });
      } else {
        challenger.push(currentUsers[socket.id]);
        challengee.push(name);
        let challengeeID = currentUsersID[name];
        io.sockets.emit('message', currentUsers[socket.id] + ' has challenge ' + name + ' to a match of Otete');
        io.sockets.socket(challengeeID).emit('challenge', 'Type /a to accept and /r to reject. You have 15s to response');
        //send an event for title to be updated for everyone
        io.sockets.emit('challenge.init', {
          player1: currentUsers[socket.id],
          player2: name
        });
      }
    }
  });
}

function handleChallengesRespones(socket) {
  socket.on('challengeRespone', function(response) {
    if(challengee.length < 1 || currentUsers[socket.id] !== challengee[0]) {
      socket.emit('challengeResult', {
        result: 'fail',
        message: 'Noone challenge you sir'
      });
    } else {
      if(response.result == 'reject') {
        io.sockets.emit('message', challengee[0] + ' has ccnly reject the challenge');
        socket.emit('recieve result');
        challengee.length = 0;
        challenger.length = 0;
        io.sockets.emit('matchCancelled');
      } else if(response.result == 'accept') {
        //io.sockets.emit('message', challengee[0] + ' has bravely accept the challenge');
        //io.sockets.emit('message', 'Match Begin!!! Each player have 1 minute to prepare');
        socket.emit('recieve result');
        gladiators.push(challenger[0]);
        gladiators.push(challengee[0]);
        let emitObj = {
          player1: challenger[0],
          player2: challengee[0]
        };
        console.log(emitObj);
        challengee.length = 0;
        challenger.length = 0;
        console.log(emitObj);
        io.sockets.emit('matchStart', emitObj);
      }
    }
  });
}

function handleMatch(socket) {
  let winner,
      winner_streak,
      loser_streak,
      winner_hand,
      loser_hand,
      need_last_hit,
      lasthitIndex;
  socket.on('hand response', function(hand) {
    let index = gladiators.indexOf(currentUsers[socket.id]);
    if(index == -1) {
      socket.emit('not in match hand');
    } else {
      socket.emit('hand receive');
      hands[index] = hand;
    }
    if(hands[0] && hands[1]) {
      let handResult = calculateHands();
      winner = handResult[0];
      if(winner == 'Draw') {
          io.sockets.emit(
              'match result',
              {
                  type: 'Draw',
                  player1: gladiators[0],
                  player2: gladiators[1],
                  hands: hands[0]
              }
          );
      } else {
          need_last_hit = handResult[1];
          lasthitIndex = handResult[2];
          let index = gladiators.indexOf(winner);
          let loser = gladiators[1 - index];
          winner_hand = hands[index];
          loser_hand = hands[1 - index];
          User.updateLose(loser, function(err, data) {
            //handlePersonalRecordName(loser);
            //handleRecordsAll();
          });
          User.updateWin(winner, function(err, data) {
            //handlePersonalRecordName(winner);
            //handleRecordsAll();
          });
          User.updateStreakWin(winner, function(err, winner_data) {
            if(err) throw err;
            winner_streak = winner_data.streak;
            //handlePersonalRecordName(winner);
            User.updateStreakLose(loser, function(err, loser_data, oldStreak) {
              if(err) throw err;
              loser_streak = oldStreak;
              io.sockets.emit(
                  'match result',
                  {
                    type: 'Not Draw',
                    winner: winner,
                    loser: loser,
                    winner_streak: winner_streak,
                    loser_streak: loser_streak,
                    winner_hand: winner_hand,
                    loser_hand: loser_hand,
                    need_last_hit: need_last_hit,
                    last_hit_at: lasthitIndex
                  }
              );
              //handlePersonalRecordName(loser);
            });
          });
      }
      hands.length = 0;
      gladiators.length = 0;
    }
  });
}

function handleTimeOut(socket) {
  socket.on('timeout', function() {
    challenger.length = 0;
    challengee.length = 0;
    gladiators.length = 0;
    hands.length = 0;
    io.sockets.emit('message', currentUsers[socket.id] + ' failed to response. Process cancel');
    io.sockets.emit('matchCancelled');
  });
}

function handleOnlineUsers() {
  io.sockets.emit('onlineUsers', currentUsers);
}

function handleRecordsAll() {
    User.findAll(function (err, data) {
        if(err) throw err;
        data.sort(function(a, b) {
          return (a.win * Math.pow(winningRate(a), 1.4) - b.win * Math.pow(winningRate(b), 1.4));
        });
        data.reverse(data);
        console.log(data);
        io.sockets.emit('updateranking', data);
    });

    function winningRate(a) {
        if((a.win + a.lose) == 0) {
            return 1;
        } else {
            return a.win / (a.win + a.lose);
        }
    }
}

function handleRecords(socket) {
  User.findAll(function (err, data) {
      if(err) throw err;
      data.sort(function(a, b) {
          return (a.win * Math.pow(winningRate(a), 1.4) - b.win * Math.pow(winningRate(b), 1.4));
      });
      data.reverse(data);
      console.log(data);
      socket.emit('updateranking', data);
  });

  function winningRate(a) {
      if((a.win + a.lose) == 0) {
          return 1;
      } else {
          return a.win / (a.win + a.lose);
      }
  }
}

function handlePersonalRecord(socket) {
  User.findYourRecord(currentUsers[socket.id], function(err, data) {
    if(err) throw err;
    socket.emit('updatepersonal', data[0]);
  });
}

function handlePersonalRecordName(username) {
  User.findYourRecord(username, function(err, data) {
    if(err) throw err;
    io.sockets.socket(currentUsersID[username]).emit('updatepersonal', data[0]);
  });
}

function handleUpdateRequest(socket){
  socket.on('requestUpdate', function() {
    handleRecords(socket);
    handlePersonalRecord(socket);
  });
}

function calculateHands() {
  let sum = 0,
      lasthitIndex = -1,
      last;
  let first_hands = hands[0].split(" ");
  let second_hands = hands[1].split(" ");
  for(let i = 0; i < first_hands.length; i++) {
    let point = compareHand(first_hands[i], second_hands[i]);
    if(point != 0) {
      sum += point;
      last = point;
      lasthitIndex = i;
    }
  }
  if(sum < 0) {
    return [gladiators[1], false, lasthitIndex];
  } else if(sum > 0) {
    return [gladiators[0], false, lasthitIndex];
  } else {
    if(lasthitIndex == -1) {
      return ['Draw', false, lasthitIndex];
    } else {
      if(last > 0) {
        return [gladiators[0], true, lasthitIndex];
      } else {
        return [gladiators[1], true, lasthitIndex];
      }
    }
  }
}

function compareHand(hand1, hand2) {
  if(hand1 == hand2) {
    return 0;
  } else if (hand1 == 'r') {
    if(hand2 == 'p') {
      return -1;
    } else {
      return 1;
    }
  } else if(hand1 == 'p') {
    if(hand2 == 'r') {
      return 1;
    } else {
      return -1;
    }
  } else {
    if(hand2 == 'p') {
      return 1;
    } else {
      return -1;
    }
  }
}

function transformHand(hand) {
  let sum = '';
  let parts = hand.split(' ');
  for(let i = 0; i < 5; i++) {
    switch(parts[i]) {
      case 'r':
        sum += '<img src="http://elasticbeanstalk-us-west-2-254492908186.s3-website-us-west-2.amazonaws.com/home/ott_platform_static_files/images/punch.png"/>';
        break;
      case 'p':
        sum += '<img src="http://elasticbeanstalk-us-west-2-254492908186.s3-website-us-west-2.amazonaws.com/home/ott_platform_static_files/images/hi.png"/>';
        break;
      case 's':
        sum += '<img src="http://elasticbeanstalk-us-west-2-254492908186.s3-website-us-west-2.amazonaws.com/home/ott_platform_static_files/images/fingerscrossed.png"/>';
    }
  }
  return sum;
}

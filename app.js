
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes/routes');
var http = require('http');
var path = require('path');
var ottServer = require('./lib/ott_server');
var app = express();

// all environments
app.set('port', process.env.PORT || 8080);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.static(path.join(__dirname, 'public')));
app.use(app.router);

// development only
if ('production' != app.get('env')) {
  app.use(express.errorHandler());
}
app.get('/', routes.login);
app.post('/login', routes.index);

var server = http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

ottServer.listen(server);
